from rest_framework import serializers

from shared.utils import random_with_N_digits
from .models import PasswordResetToken, UserToken, User
from userToken.models import UserToken
from user.models import User
from user.serializers import CustomTokenObtainSerializer, EmailValidator, CreateUserSerializers
from user.enum import UserType
class UserTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserToken
        fields = ('user', 'user_token_id', 'code', 'experied')

class CreateUserTokenSerializers(serializers.Serializer):
    email = serializers.EmailField(
        required=True,
        validators=[EmailValidator()]
    )

    user_type = serializers.ChoiceField(
        required=True,
        choices=UserType.choices
    )
    
    def validate(self, data):
        email = data.get('email')
        user_type = data.get('user_type')

        if not User.objects.filter(email=email, user_type=user_type).exists():
            raise serializers.ValidationError("User with email and user_type does not exist.")
        
        return data

class PasswordResetTokenSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(
        style={'input_type': 'password'},
        write_only=True,
        required=True,
    )

    class Meta:
        model = PasswordResetToken
        # fields = ('email', 'new_password')
        fields = ('user_token_id', 'email', 'code', 'new_password')

    def validate_email(self, value):
        try:
            user = User.objects.get(email=value)
        except User.DoesNotExist:
            raise serializers.ValidationError(_('User with this email does not exist.'))

        if not PasswordResetToken.objects.filter(user=user).exists():
            raise serializers.ValidationError(_('Password reset token does not exist.'))

        return value
class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(max_length=128, required=True)
    new_password = serializers.CharField(max_length=128, required=True)