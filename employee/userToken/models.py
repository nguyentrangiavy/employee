from django.db import models
from user.models import User

# Create your models here.
class UserToken(models.Model):
    email = User.email
    user_type = User.user_type
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_token_id = models.CharField(max_length=255, null = False)
    code = models.CharField(max_length=6, null=False)
    experied = models.DateTimeField(null = False)
    
class PasswordResetToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

