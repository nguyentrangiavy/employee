import pytz
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.generics import ListCreateAPIView
from shared.utils import format_response, random_with_N_digits
from user.models import User
from userToken.serializers import ChangePasswordSerializer, UserTokenSerializer, CreateUserTokenSerializers, PasswordResetTokenSerializer
from userToken.models import PasswordResetToken, UserToken
from django.utils import timezone
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from django.contrib.auth import update_session_auth_hash
from django.utils.translation import gettext_lazy as _



class ListCreateUserTokenView(ListCreateAPIView):
    model = UserToken
    serializer_class = UserTokenSerializer
    def list(self, request, *args, **kwargs):
        user_token = UserToken.objects.filter() #filter: dk lay data
        serializer = UserTokenSerializer(user_token, many = True)
        response = format_response(
            success=True,
            status = status.HTTP_200_OK,
            message = "Get succses",
            data = serializer.data
        )
        return Response(response, status = response.get('status'))




class CheckView(generics.GenericAPIView):
    serializer_class = CreateUserTokenSerializers 

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data['email']
        user_type = serializer.validated_data['user_type']

        users = User.objects.filter(
            email=email,
            user_type=user_type
        )
        if not users.exists():
            response_data = format_response(
                success=False,
                status=status.HTTP_400_BAD_REQUEST,
                message=f"User does not exist with email {email}",
            )
            return Response(response_data, status=response_data.get('status'))

        user = users.first()
        user_token_id = "exnodes"
        user_token, created = UserToken.objects.get_or_create(
            user=user,
            user_token_id=user_token_id,
            defaults={
                'code': str(random_with_N_digits(6)),
                'experied': timezone.now() + timezone.timedelta(minutes=15)  # Chính sửa từ expired thành expires
            }
        )

        if not created:
            user_token.code = str(random_with_N_digits(6))
            user_token.experied = timezone.now() + timezone.timedelta(minutes=15)  # Chính sửa từ expired thành expires
            user_token.save()

        response_data = format_response(
            success=True,
            status=status.HTTP_200_OK,
            message="User exists.",
            data={
                'user_token_id': user_token.user_token_id,
                'code': user_token.code,
            }
        )

        return Response(response_data, status=response_data.get('status'))





class PasswordResetView(APIView):
    def put(self, request, *args, **kwargs):
        user_token_id = request.data.get('user_token_id')
        code = request.data.get('code')
        new_password = request.data.get('new_password')

        try:
            user_token = UserToken.objects.get(user_token_id=user_token_id)
        except UserToken.DoesNotExist:
            return Response(
                {'success': False},
                status=status.HTTP_400_BAD_REQUEST
            )
        
        if user_token.code != code:
            return Response(
                {'success': False, 'message': 'Mã xác minh không hợp lệ', 'code1' :user_token.code ,'code2' : code},
                status=status.HTTP_400_BAD_REQUEST
            )

        if user_token.experied < timezone.now():
            return Response(
                {'success': False, 'message': 'Mã xác minh đã hết hạn'},
                status=status.HTTP_400_BAD_REQUEST
            )

        user = user_token.user
        user.set_password(new_password)
        user.save()

        user_token.delete()

        return Response({'success': True, 'message': 'Password reset successful'}, status=status.HTTP_200_OK)

    
class ChangePasswordView(APIView):
    permission_classes = [IsAuthenticated]
    def put(self, request, format=None):
        serializer = ChangePasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        old_password = serializer.validated_data['old_password']
        new_password = serializer.validated_data['new_password']
        user = request.user
        if not user.check_password(old_password):
            return Response(
                {'success': False, 'error': _('Invalid old password.')},
                status=status.HTTP_400_BAD_REQUEST,
            )
        user.set_password(new_password)
        user.save()
        return Response({'success': 'Password chnaged successful'}, status=status.HTTP_200_OK)