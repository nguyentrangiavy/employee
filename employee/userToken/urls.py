from django.urls import path
from userToken import views

urlpatterns = [
    path('userToken/', views.ListCreateUserTokenView.as_view()),
    path('check/', views.CheckView.as_view(), name='check_view'),
    path('reset-password/', views.PasswordResetView.as_view(), name='password_reset_view'),
    path('change/', views.ChangePasswordView.as_view(), name='change_view'),
]
