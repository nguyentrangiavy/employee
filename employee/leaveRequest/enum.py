from django.db import models

class Reason(models.TextChoices):
    SICK = "SICK"
    VACATION = "VACATION"
    FAMILY = "FAMILY"
    ACCIDENT = "ACCIDENT"
    NO_PERMISSION = "NO_PERMISSION"
    OTHER = "OTHER"

class Status(models.TextChoices):
    CHECKD = "CHECKED"
    NOT_CHECKD = "NOT_CHECKED"
